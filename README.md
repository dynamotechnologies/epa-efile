# EPA-efile README

### To build the app
from the project root directory:
`ant`

### To clean the app
this will delete the /build and /dist directories
 
from the project root directory:
`ant clean`


#### extra notes
it is recommended to perform `ant clean` prior to an `ant` build command

standard java build stuff:
build command will compile source files and place compiled java files in the /build folder along with any dependencies the buildscript may specify to include (other jars etc.) 
then it will zip everything up in /build, add a MANIFEST file to it --> bundle it all into a jar file placed in /dist. This resultant jar is your final EPA-eFile build artifact.