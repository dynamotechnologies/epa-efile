/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epa.efile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.namespace.QName;
import net.exchangenetwork.schema.node._2.DocumentFormatType;
import net.exchangenetwork.wsdl.node._2.NodeFaultMessage;
import javax.xml.ws.soap.MTOMFeature;

/**
 *
 * @author mhsu
 */
public class EPAEFile {

    public static String endpoint = "MISSING";    // Service endpoint, to be loaded from prop file

    /**
     * @param args the command line arguments
     *
     * java -jar EPA-eFile.jar [efile.data]
     *
     * This app should be installed under /var/www/common/efile
     * See the INSTALL file for details
     *
     * Configs are read from the local EPA-eFile.properties file
     * Sample properties file:
     *
     *   endpoint=https://devngn.epacdxnode.net/ngn-enws20/services/NetworkNode2Service
     *   userid=mhsu@phaseonecg.com
     *   password=CDXmhsu16
     *
     * If authtoken is an available property, use that instead of authorizing with userid/password
     */
    public static void main(String[] args) {

        String domain = "";
        String authenticationMethod = "password";

        String action = "";
        String efiledata = "";
        if (args.length == 1) {
            action = args[0];
            efiledata = "efile.data";
        } else if (args.length == 2) {
            action = args[0];
            efiledata = args[1];
        } else {
            usage();
        }

        String propfile = "EPA-eFile.properties";
        Properties configFile = new Properties();
        try {
            configFile.load(EPAEFile.class.getClassLoader().getResourceAsStream(propfile));
        } catch (Exception e) {
            die("Error opening properties file " + propfile + ": " + e.getMessage());
        }

        EPAEFile.endpoint = configFile.getProperty("endpoint");
        String userid = configFile.getProperty("userid");
        String password = configFile.getProperty("password");
        String token = configFile.getProperty("authtoken");

        epa.efile.EPAEFile efile = new EPAEFile();

        if (EPAEFile.endpoint == null) {
            die("Error: No endpoint given!");
        }


        if (action.equals("-ping")) {
            /*
             *  Ping node service
             */
            String hello = "hello world";
            javax.xml.ws.Holder<net.exchangenetwork.schema.node._2.NodeStatusCode> status = new javax.xml.ws.Holder();
            javax.xml.ws.Holder<String> detail = new javax.xml.ws.Holder();
            try {
                EPAEFile.nodePing(hello, status, detail);
                System.out.println("Ping completed, status is: " + status.value + ", detail is: " + detail.value );
            } catch (net.exchangenetwork.wsdl.node._2.NodeFaultMessage e) {
                die("Exception: " + e.getMessage());
            }

        } else if (action.equals("-auth")) {
            /*
             *  Get auth token
             */
            if (token == null) {
                try {
                    token = EPAEFile.authenticate(userid, password, domain, authenticationMethod);
                } catch (NodeFaultMessage e) {
                    die("Error in authentication: " + e.getFaultInfo().getDescription());
                }
            }
            System.out.println(token);

        } else if (action.equals("-submit")) {
            /*
             *  Submit files
             */
            if (token == null) {
                try {
                    token = EPAEFile.authenticate(userid, password, domain, authenticationMethod);
                } catch (NodeFaultMessage e) {
                    die("Error in authentication: " + e.getFaultInfo().getDescription());
                }
            }
            String txid = efile.submitFiles(efiledata, token);
            System.out.println(txid);

        } else if (action.equals("-status")) {
          if (token == null) {
            try {
              token = EPAEFile.authenticate(userid, password, domain, authenticationMethod);
            } catch (NodeFaultMessage e) {
              die("Error in authentication: " + e.getFaultInfo().getDescription());
            }
            String[] statusAndDetails = new String[0];
            try {
              statusAndDetails = efile.getStatus(efiledata, token);
            } catch (NodeFaultMessage e) {
              die("Failed to retrieve status for txid: " + efiledata);
            }
            System.out.println(statusAndDetails[0] + ";" + "Details: " +
                                statusAndDetails[1]);
          }
        } else {
            die("Unrecognized command " + action);
        }
    }

    private static void usage() {
        die("Usage: java -jar EPA-eFile.jar -ping|-auth|-submit|-status [efile.data]");
    }

    private static void die(String errmsg) {
        System.err.println(errmsg);
        System.exit(1);
    }

    /*
     * submitFiles: transmit a set of files to EPA over the Node2 network
     *
     * filespec - path to a text file containing a list of files to submit (see below)
     * securityToken - authorization string provided by a previous authenticate() action
     *                 (or permanent key provided by helpdesk)
     *
     * The filespec is a UTF-8 text file containing a series of vbar-delimited fields
     * in the format: filename|path_to_file|mimetype
     *
     * All fields must be URL-encoded (e.g. spaces become %20)
     *
     *      filename - the name of the file, must include a dot suffix
     *      path_to_file - path to file, does not have to match filename argument
     *      mimetype - Either application/pdf or text/csv
     */
    private String submitFiles(String filespec, String securityToken) {
        javax.xml.ws.Holder<java.lang.String> transactionId = new javax.xml.ws.Holder();                                    // Response txid
        java.lang.String dataflow = "e-NEPA";
        java.lang.String flowOperation = "default";
        java.util.List<java.lang.String> recipient = new ArrayList();
        java.util.List<net.exchangenetwork.schema.node._2.NotificationURIType> notificationURI = new ArrayList();
        java.util.List<net.exchangenetwork.schema.node._2.NodeDocumentType> documents = new ArrayList();
        javax.xml.ws.Holder<net.exchangenetwork.schema.node._2.TransactionStatusCode> status = new javax.xml.ws.Holder();   // Response code
        javax.xml.ws.Holder<java.lang.String> statusDetail = new javax.xml.ws.Holder();

        BufferedReader fp = null;
        try {
            fp = new BufferedReader(new FileReader(filespec));
        } catch (Exception e) {
            die("Error opening file spec: " + e.getMessage());
        }

        try {
            String line;
            while ((line = fp.readLine()) != null) {
                String parts[] = line.split("\\|");
                if (parts.length != 3) {
                    die("File spec has " + parts.length + "/3 fields: " + line);
                }
                String filename = URLDecoder.decode(parts[0], "UTF-8");
                String path_to_file = URLDecoder.decode(parts[1], "UTF-8");
                String mimetype = URLDecoder.decode(parts[2], "UTF-8");

                File f = new File(path_to_file);
                if (f.exists() == false) {
                    die("Missing file " + path_to_file);
                }
                net.exchangenetwork.schema.node._2.NodeDocumentType doc;
                doc = registerDocument(filename, mimetype, path_to_file);
                documents.add(doc);
            }
        } catch (IOException e) {
            die("Error reading file spec: " + e.getMessage());
        }

        try {
            EPAEFile.submit(securityToken, transactionId, dataflow, flowOperation, recipient, notificationURI, documents, status, statusDetail);
        } catch (NodeFaultMessage e) {
            die("Error submitting files: " + e.getMessage() + " / " + e.getFaultInfo().getDescription());
        }

        return transactionId.value;
    }

    /*
     * Utility method to package a single document for transmission
     */
    private net.exchangenetwork.schema.node._2.NodeDocumentType registerDocument(String name, String mimetype, String path) {
        net.exchangenetwork.schema.node._2.NodeDocumentType doc = new net.exchangenetwork.schema.node._2.NodeDocumentType();
        net.exchangenetwork.schema.node._2.AttachmentType content = new net.exchangenetwork.schema.node._2.AttachmentType();

        content.setContentType(mimetype);   // e.g. text/plain, application/pdf
        DataHandler data = new DataHandler(new FileDataSource(path));
        content.setValue(data);

        doc.setDocumentName(name);  // Make sure this includes a dot suffix
        doc.setDocumentFormat(DocumentFormatType.BIN);
        doc.setDocumentContent(content);

        return doc;
    }

    private static String authenticate(java.lang.String userId, java.lang.String credential, java.lang.String domain, java.lang.String authenticationMethod) throws NodeFaultMessage {
        URL endpointURL = null;
        try {
            endpointURL = new URL(EPAEFile.endpoint);
        } catch (java.net.MalformedURLException e) {
            die("Malformed endpoint URL!!! " + EPAEFile.endpoint);
        }
        QName qname = new QName("http://www.exchangenetwork.net/wsdl/node/2","NetworkNode2");

        net.exchangenetwork.wsdl.node._2.NetworkNode2 service = new net.exchangenetwork.wsdl.node._2.NetworkNode2(endpointURL, qname);
        net.exchangenetwork.wsdl.node._2.NetworkNodePortType2 port = service.getNetworkNodePort2();
        return port.authenticate(userId, credential, domain, authenticationMethod);
    }

    private static void submit(java.lang.String securityToken, javax.xml.ws.Holder<java.lang.String> transactionId, java.lang.String dataflow, java.lang.String flowOperation, java.util.List<java.lang.String> recipient, java.util.List<net.exchangenetwork.schema.node._2.NotificationURIType> notificationURI, java.util.List<net.exchangenetwork.schema.node._2.NodeDocumentType> documents, javax.xml.ws.Holder<net.exchangenetwork.schema.node._2.TransactionStatusCode> status, javax.xml.ws.Holder<java.lang.String> statusDetail) throws NodeFaultMessage {
        URL endpointURL = null;
        try {
            endpointURL = new URL(EPAEFile.endpoint);
        } catch (java.net.MalformedURLException e) {
            die("Malformed endpoint URL!!! " + EPAEFile.endpoint);
        }
        QName qname = new QName("http://www.exchangenetwork.net/wsdl/node/2","NetworkNode2");
        //enable MTOM
        MTOMFeature mtom = new MTOMFeature(true, 2048);
        net.exchangenetwork.wsdl.node._2.NetworkNode2 service = new net.exchangenetwork.wsdl.node._2.NetworkNode2(endpointURL, qname);

        net.exchangenetwork.wsdl.node._2.NetworkNodePortType2 port = service.getNetworkNodePort2(mtom);
        port.submit(securityToken, transactionId, dataflow, flowOperation, recipient, notificationURI, documents, status, statusDetail);
    }

    private static void nodePing(java.lang.String hello, javax.xml.ws.Holder<net.exchangenetwork.schema.node._2.NodeStatusCode> nodeStatus, javax.xml.ws.Holder<java.lang.String> statusDetail) throws NodeFaultMessage {
        URL endpointURL = null;
        try {
            endpointURL = new URL(EPAEFile.endpoint);
        } catch (java.net.MalformedURLException e) {
            die("Malformed endpoint URL!!! " + EPAEFile.endpoint);
        }
        QName qname = new QName("http://www.exchangenetwork.net/wsdl/node/2","NetworkNode2");

        net.exchangenetwork.wsdl.node._2.NetworkNode2 service = new net.exchangenetwork.wsdl.node._2.NetworkNode2(endpointURL, qname);
        net.exchangenetwork.wsdl.node._2.NetworkNodePortType2 port = service.getNetworkNodePort2();
        port.nodePing(hello, nodeStatus, statusDetail);
    }

    private static String[] getStatus(String txidS, String token) throws NodeFaultMessage {

      javax.xml.ws.Holder<String> txid = new javax.xml.ws.Holder(txidS);
      javax.xml.ws.Holder<net.exchangenetwork.schema.node._2.TransactionStatusCode> status = new javax.xml.ws.Holder();   // Response code
      javax.xml.ws.Holder<String> statusDetail = new javax.xml.ws.Holder();

      URL endpointURL = null;
      try {
          endpointURL = new URL(EPAEFile.endpoint);
      } catch (java.net.MalformedURLException e) {
          EPAEFile.die("Malformed endpoint URL!!! " + EPAEFile.endpoint);
      }
      QName qname = new QName("http://www.exchangenetwork.net/wsdl/node/2","NetworkNode2");

      net.exchangenetwork.wsdl.node._2.NetworkNode2 service = new net.exchangenetwork.wsdl.node._2.NetworkNode2(endpointURL, qname);
      net.exchangenetwork.wsdl.node._2.NetworkNodePortType2 port = service.getNetworkNodePort2();
      port.getStatus(token, txid, status, statusDetail);

      return new String[] {status.value.toString(), statusDetail.value};
    }

}
